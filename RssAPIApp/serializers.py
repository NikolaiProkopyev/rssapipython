from rest_framework import serializers

from RssAPIApp.models import RssItem


class RssItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RssItem
        fields = ('id', 'source', 'title', 'link', 'date')
