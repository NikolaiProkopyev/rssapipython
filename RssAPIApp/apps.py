from django.apps import AppConfig


class RssapiappConfig(AppConfig):
    name = 'RssAPIApp'
