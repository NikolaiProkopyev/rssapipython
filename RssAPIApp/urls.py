from django.urls import path

from RssAPIApp.views import RssItemList, RssItemDetail

rss_api_patterns = ((
    path('rssitem/', RssItemList.as_view()),
    path('rssitem/<int:pk>', RssItemDetail.as_view()),
), 'rss_api')
