from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from RssAPIApp.models import RssItem
from RssAPIApp.serializers import RssItemSerializer


class RssItemList(APIView):
    def get(self, request):
        rss_items = RssItem.objects.all()
        rss_items_serializer = RssItemSerializer(instance=rss_items, many=True)
        return Response(rss_items_serializer.data)

    def post(self, request):
        rss_item_serializer = RssItemSerializer(data=request.data)
        if rss_item_serializer.is_valid():
            rss_item_serializer.save()
        return Response(rss_item_serializer.data)


class RssItemDetail(APIView):
    def get(self, request, pk):
        try:
            rss_item = RssItem.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        rss_item_serializer = RssItemSerializer(instance=rss_item)
        return Response(rss_item_serializer.data)

    def put(self, request, pk):
        try:
            rss_item = RssItem.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        rss_item_serializer = RssItemSerializer(instance=rss_item, data=request.data, partial=True)
        if rss_item_serializer.is_valid():
            rss_item_serializer.save()
        return Response(rss_item_serializer.data)

    def delete(self, request, pk):
        try:
            rss_item = RssItem.objects.get(id=pk)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        rss_item_serializer = RssItemSerializer(instance=rss_item)
        rss_item.delete()
        return Response(rss_item_serializer.data)
