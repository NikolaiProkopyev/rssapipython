from django.db import models


# Create your models here.
class RssItem(models.Model):
    class RssSource(models.IntegerChoices):
        UNDEFINED = 0, 'Не определено'
        PROKAZAN = 1, 'ProKazan'
        RT_ONLINE = 2, 'RT Online'
        BUSINESS_GAZETA = 3, 'Business Gazeta'

    id = models.BigAutoField(primary_key=True)

    source = models.IntegerField(
        choices=RssSource.choices,
        db_column="source",
        verbose_name="Источник"
    )

    title = models.TextField(
        db_column="title",
        verbose_name="Заголовок"
    )

    link = models.TextField(
        db_column="link",
        verbose_name="Ссылка"
    )

    date = models.DateTimeField(
        db_column="date",
        verbose_name="Дата публикации"
    )

    class Meta:
        managed = False
        db_table = "rss_item"
        verbose_name = "RSS запись"
        verbose_name_plural = "RSS записи"
